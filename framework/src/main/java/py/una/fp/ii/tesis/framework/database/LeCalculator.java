package py.una.fp.ii.tesis.framework.database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Marco Alvarez
 */
public class LeCalculator {

    private final Database database;
    private final String scheme;

    public LeCalculator(String scheme) {
        this.database = new Database();
        this.scheme = scheme;
    }

    public void updateLeFromFile(String pathname) throws SQLException {
        Connection dbConnection = null;
        Statement statementHM = null;
        Statement statementEdges = null;
        PreparedStatement preparedStatement = null;
        String vertexIdFromIncidents = "select COUNT(id) as id, vertex_id from  " + scheme + ".incidents group by vertex_id order by COUNT(id) desc";
        String selectEdge = "select id, origin_id, destination_id from  " + scheme + ".edge";
        String updateEdgeLe = "UPDATE  " + scheme + ".edge SET violence_level = ? where id = ?";
        Map<Integer, Integer> vertexIdAmountMap = new HashMap<>();
        try {
            dbConnection = database.getDBConnection();
            statementHM = dbConnection.createStatement();

            File file = new File(pathname); //"D:\\violence_level_asociated_asuncion.csv"
            InputStream fileInputStream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream));
            String strLine;
            Map<Integer, Double> leMap = new HashMap<>();
            while ((strLine = br.readLine()) != null) {
                String[] split = strLine.split(",");
                String valor = split[0];
                String prof = split[1];
                leMap.put(Integer.valueOf(valor), Double.valueOf(prof));
            }
            br.close();
            System.out.println("Map Size: " + leMap.size());

            ResultSet rs = statementHM.executeQuery(vertexIdFromIncidents);
            while (rs.next()) {
                vertexIdAmountMap.put(rs.getInt("vertex_id"), rs.getInt("id"));
            }

            statementEdges = dbConnection.createStatement();
            rs = statementEdges.executeQuery(selectEdge);
            while (rs.next()) {
                int id = rs.getInt("id");
                int originId = rs.getInt("origin_id");
                int destinationId = rs.getInt("destination_id");

                int originAmount = vertexIdAmountMap.getOrDefault(originId, 0);
                int destinationAmount = vertexIdAmountMap.getOrDefault(destinationId, 0);
                int total = originAmount + destinationAmount;

                preparedStatement = dbConnection.prepareStatement(updateEdgeLe);
                preparedStatement.setDouble(1, leMap.get(total));
                preparedStatement.setInt(2, id);
                preparedStatement.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (statementEdges != null) {
                statementEdges.close();
            }
            if (statementHM != null) {
                statementHM.close();
            }
            if (preparedStatement != null) {
                preparedStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
    }
}
