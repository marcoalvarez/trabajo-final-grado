package py.una.fp.ii.tesis.framework.exec;

import py.una.fp.ii.tesis.framework.database.ResourceVelocities;

import java.time.LocalDateTime;

public class MainUpdVelocities {

    public static void main(String... args) {

        validateInput(args);

        System.out.println("Init process time: " + LocalDateTime.now());
        ResourceVelocities t = new ResourceVelocities(args[0]);
        t.run(
                Double.parseDouble(args[1]), //patrolVelocity
                Double.parseDouble(args[2]), //motorcycleVelocity
                Double.parseDouble(args[3]) //officialVelocity
        );
        System.out.println("End process time: " + LocalDateTime.now());
    }

    private static void validateInput(String... args) throws IllegalArgumentException {
        if (args.length < 4) {
            throw new IllegalArgumentException("Four args needed: scheme, patrolVelocity, motorcycleVelocity, officialVelocity");
        }
        try {
            Double.parseDouble(args[1]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("patrolVelocity arg is not a number");
        }
        try {
            Double.parseDouble(args[2]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("motorcycleVelocity arg is not a number");
        }
        try {
            Double.parseDouble(args[3]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("officialVelocity arg is not a number");
        }
    }
}
