package py.una.fp.ii.tesis.framework.algorithms;

import py.una.fp.ii.tesis.framework.database.QueriesForCoverage;
import py.una.fp.ii.tesis.framework.models.CycleResult;
import py.una.fp.ii.tesis.framework.models.PoliceResource;

import java.util.ArrayList;
import java.util.List;

/**
 * @author marco
 */
public class RouteCalculation implements Runnable {

    private final List<Integer> coveredEdgeTMaxList = new ArrayList<>();
    private final Integer initialVertex;
    private final PoliceResource policeResource;
    private final int TMax;
    private final List<Integer> coveredVertexTMaxList = new ArrayList<>();
    private final QueriesForCoverage queriesForCoverage;

    public RouteCalculation(Integer vertex, PoliceResource policeResource, int tMax, QueriesForCoverage queriesForCoverage) {
        this.initialVertex = vertex;
        this.policeResource = policeResource;
        this.TMax = tMax;
        this.queriesForCoverage = queriesForCoverage;
    }

    @Override
    public void run() {
        List<Integer> cycleVertexTMaxList = new ArrayList<>();
        cycleVertexTMaxList.add(initialVertex);
        findRoute(0, initialVertex, new ArrayList<>(), cycleVertexTMaxList);
        CycleResult cycleResult = new CycleResult(initialVertex, coveredEdgeTMaxList, coveredVertexTMaxList, queriesForCoverage);
        cycleResult.calculateViolenceLevel();
        cycleResult.setPoliceResource(policeResource);
        //Guarda cobertura aqui
        try {
            int idResult = queriesForCoverage.insertResults(cycleResult);
            queriesForCoverage.insertResultsVertex(idResult, cycleResult.getCoveredVertexTMaxList());
            queriesForCoverage.insertResultsEdge(idResult, cycleResult.getCoveredEdgeTMaxList());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void findRoute(double currentTime, Integer currentVertex, List<Integer> cycleCoveredEdgeTMax, List<Integer> cycleVertexTMaxList) {
        List<Object[]> eX = queriesForCoverage.getEdgeListByVertex(currentVertex);
        for (Object[] edge : eX) {
            Integer destinationVertex = (Integer) edge[1];
            double t = 0.0;
            switch (policeResource.getType()) {
                case 1:
                    t = (Double) edge[2];
                    break;
                case 2:
                    t = (Double) edge[3];
                    break;
                case 3:
                    t = (Double) edge[4];
            }
            if (t != -1) {
                Integer edgeTurn = queriesForCoverage.getEdgeByOriginIdDestinationId(destinationVertex, currentVertex);
                if ((currentTime + t) < TMax && !cycleCoveredEdgeTMax.contains((Integer) edge[0])) {

                    cycleCoveredEdgeTMax.add((Integer) edge[0]);
                    cycleCoveredEdgeTMax.add(edgeTurn);
                    if (!cycleVertexTMaxList.contains(destinationVertex)) {
                        cycleVertexTMaxList.add(destinationVertex);
                    }

                    findRoute(currentTime + t, destinationVertex, cycleCoveredEdgeTMax, cycleVertexTMaxList);

                }

            }
        }
        cycleCoveredEdgeTMax.forEach(e -> {
            if (!coveredEdgeTMaxList.contains(e)) {
                coveredEdgeTMaxList.add(e);
            }
        });
        cycleVertexTMaxList.forEach(v -> {
            if (!coveredVertexTMaxList.contains(v)) {
                coveredVertexTMaxList.add(v);
            }
        });
    }


}


