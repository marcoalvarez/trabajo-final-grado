package py.una.fp.ii.tesis.framework.exec;

import py.una.fp.ii.tesis.framework.database.LeCalculator;

import java.time.LocalDateTime;

public class MainLeCalc {

    public static void main(String[] args) {

        validateInput();

        LeCalculator leCalculator = new LeCalculator(args[0]);

        System.out.println("Inicia proceso: " + LocalDateTime.now());
        try {
            leCalculator.updateLeFromFile(args[1]);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Finaliza proceso: " + LocalDateTime.now());
    }

    private static void validateInput(String... args) throws IllegalArgumentException {
        if (args.length < 2) {
            throw new IllegalArgumentException("Two args needed: scheme, pathname");
        }
    }
}
