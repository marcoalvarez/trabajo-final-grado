package py.una.fp.ii.tesis.framework.exec;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import py.una.fp.ii.tesis.framework.algorithms.TabuSearchA3;
import py.una.fp.ii.tesis.framework.database.Database;
import py.una.fp.ii.tesis.framework.database.QueriesForA2A3;
import py.una.fp.ii.tesis.framework.models.ResourceVertex;
import py.una.fp.ii.tesis.framework.models.Solution;
import py.una.fp.ii.tesis.framework.models.SolutionForFile;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.MOTORCYCLE;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.OFFICIAL;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.PATROL;

/**
 * @author Marco Alvarez
 */
public class MainA3 {

    private final QueriesForA2A3 queriesForA2A3;
    private final Database database;
    String instance;

    public MainA3(String scheme, String instance) {
        this.database = new Database();
        this.queriesForA2A3 = new QueriesForA2A3(scheme, database);
        this.instance = instance;
    }

    public static void main(String... args) {

        validateInput(args);

        System.out.println("Init process time: " + LocalDateTime.now());
        for (int i = 0; i < Integer.parseInt(args[1]); i++) {
            MainA3 mainA3 = new MainA3(args[0], String.valueOf(i));
            mainA3.runA3();
            System.out.println("instancia " + i + " terminado");
        }
        System.out.println("End process time: " + LocalDateTime.now());
    }

    private static void validateInput(String... args) throws IllegalArgumentException {
        if (args.length < 2) {
            throw new IllegalArgumentException("Two args needed: scheme, executions");
        }
        try {
            Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("executions arg is not a number");
        }
    }

    public void runA3() {

        long initTime = System.currentTimeMillis();

        List<ResourceVertex> tabuList = new ArrayList<>();
        int patrolsUnits = queriesForA2A3.getResourcePoliceByType(PATROL);
        int motorCyclesUnits = queriesForA2A3.getResourcePoliceByType(MOTORCYCLE);
        int officersUnits = queriesForA2A3.getResourcePoliceByType(OFFICIAL);
        int vertexSize = queriesForA2A3.getVertexSize();
        int tabuSize = (int) (vertexSize * 0.1); // 10% of all vertex
        int neighborSize = tabuSize / 2; //5% of all vertex

        TabuSearchA3 tabuSearchA3 = new TabuSearchA3(tabuList, patrolsUnits, motorCyclesUnits, officersUnits, queriesForA2A3, tabuSize, neighborSize);
        Solution initialSolution = queriesForA2A3.getInitialSolutionHotSpotA3(instance);
        Solution finalSolution = tabuSearchA3.run(initialSolution);

        long finalTime = System.currentTimeMillis();
        finalSolution.setExecutionTime((finalTime - initTime) / 1000);
        try {
            queriesForA2A3.insertSolution(finalSolution);
            printToFile(new SolutionForFile(finalSolution, queriesForA2A3));
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        database.closeDBConnection();
    }

    private void printToFile(SolutionForFile solutionForFile) {
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        try {
            FileWriter writer = new FileWriter("framework/outputs/A3-output-" + solutionForFile.getInstance() + ".json");
            gson.toJson(solutionForFile, writer);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
