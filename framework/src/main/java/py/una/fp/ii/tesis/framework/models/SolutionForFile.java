package py.una.fp.ii.tesis.framework.models;

import py.una.fp.ii.tesis.framework.database.QueriesForA2A3;

import java.util.List;
import java.util.stream.Collectors;

import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.MOTORCYCLE;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.OFFICIAL;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.PATROL;

public class SolutionForFile {
    private Double fRec;
    private Long executionTime;
    private String type;
    private String instance;
    private List<PolicePosition> positions;

    public SolutionForFile(Solution solution, QueriesForA2A3 queriesForA2A3) {
        this.fRec = solution.getFrecValue();
        this.executionTime = solution.getExecutionTime();
        this.type = solution.getType();
        this.instance = solution.getInstance();
        this.positions = solution.getResourceVertexList().stream().map(r -> new PolicePosition(r, queriesForA2A3)).collect(Collectors.toList());
    }

    public Double getfRec() {
        return fRec;
    }

    public void setfRec(Double fRec) {
        this.fRec = fRec;
    }

    public Long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Long executionTime) {
        this.executionTime = executionTime;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<PolicePosition> getPositions() {
        return positions;
    }

    public void setPositions(List<PolicePosition> positions) {
        this.positions = positions;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    private class PolicePosition {
        private String policeType;
        private double latitude;
        private double longitude;

        public PolicePosition(ResourceVertex resourceVertex, QueriesForA2A3 queriesForA2A3) {
            double[] latitudeLongitude = queriesForA2A3.getLatitudeLongitude(resourceVertex.getIdVertex());
            this.policeType = getPoliceType(resourceVertex.getResourceType());
            this.latitude = latitudeLongitude[0];
            this.longitude = latitudeLongitude[1];
        }

        public String getPoliceType() {
            return policeType;
        }

        public void setPoliceType(String policeType) {
            this.policeType = policeType;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        private String getPoliceType(Integer resourceType) {
            String result;
            switch (resourceType) {
                case PATROL:
                    result = "Patrol";
                    break;
                case MOTORCYCLE:
                    result = "Motorcycle";
                    break;
                case OFFICIAL:
                    result = "Official";
                    break;
                default:
                    result = null;
            }
            return result;
        }
    }

}

