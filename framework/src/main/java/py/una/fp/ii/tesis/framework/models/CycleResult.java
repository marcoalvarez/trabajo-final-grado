/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.una.fp.ii.tesis.framework.models;

import py.una.fp.ii.tesis.framework.database.QueriesForCoverage;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author marco
 */
public class CycleResult {

    private Double violenceLevel;
    private Integer selectedVertex;
    private PoliceResource policeResource;
    private List<Integer> coveredEdgeTMaxList;
    private List<Integer> coveredVertexTMaxList;
    private final QueriesForCoverage queriesForCoverage;

    public CycleResult(Integer selectedVertex, List<Integer> coveredEdgeTMaxList, List<Integer> coveredVertexTMaxList,
                       QueriesForCoverage queriesForCoverage) {
        this.selectedVertex = selectedVertex;
        this.coveredEdgeTMaxList = coveredEdgeTMaxList;
        this.coveredVertexTMaxList = coveredVertexTMaxList;
        this.queriesForCoverage = queriesForCoverage;
    }

    public void calculateViolenceLevel() {
        violenceLevel = queriesForCoverage.getViolenceLevel(coveredEdgeTMaxList);
    }

    public Double getViolenceLevel() {
        return violenceLevel;
    }

    public Integer getSelectedVertex() {
        return selectedVertex;
    }

    public void setSelectedVertex(Integer selectedVertex) {
        this.selectedVertex = selectedVertex;
    }

    public List<Integer> getCoveredEdgeTMaxList() {
        return coveredEdgeTMaxList;
    }

    public void addAristaCubierta(Integer aristaCubierta) {
        if (coveredEdgeTMaxList == null) {
            coveredEdgeTMaxList = new ArrayList<>();
        }
        coveredEdgeTMaxList.add(aristaCubierta);
    }

    public List<Integer> getCoveredVertexTMaxList() {
        return coveredVertexTMaxList;
    }

    public void setCoveredVertexTMaxList(List<Integer> coveredVertexTMaxList) {
        this.coveredVertexTMaxList = coveredVertexTMaxList;
    }

    public PoliceResource getPoliceResource() {
        return policeResource;
    }

    public void setPoliceResource(PoliceResource policeResource) {
        this.policeResource = policeResource;
    }

    public void setCoveredEdgeTMaxList(List<Integer> coveredEdgeTMaxList) {
        this.coveredEdgeTMaxList = coveredEdgeTMaxList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CycleResult that = (CycleResult) o;
        return Objects.equals(violenceLevel, that.violenceLevel) &&
                Objects.equals(selectedVertex, that.selectedVertex);
    }

    @Override
    public int hashCode() {

        return Objects.hash(violenceLevel, selectedVertex);
    }

    @Override
    public String toString() {
        return violenceLevel +
                "," + selectedVertex +
                "," + policeResource.getName();
    }
}
