package py.una.fp.ii.tesis.framework.models;

import java.util.Objects;
import java.util.UUID;

public class ResourceVertex {
    private final UUID uuid;
    private Integer resultId;
    private final Integer resourceType;
    private Integer idVertex;
    private Double violenceLevelCovered;
    private int tabuIteration;
    private boolean hasMoved;

    public ResourceVertex(UUID uuid, Integer resultId, Integer resourceType, Integer idVertex, Double violenceLevelCovered) {
        this.uuid = uuid;
        this.resultId = resultId;
        this.resourceType = resourceType;
        this.idVertex = idVertex;
        this.violenceLevelCovered = violenceLevelCovered;
        this.hasMoved = false;
    }

    public Integer getResultId() {
        return resultId;
    }

    public void setResultId(Integer resultId) {
        this.resultId = resultId;
    }

    public Integer getResourceType() {
        return resourceType;
    }

    public Integer getIdVertex() {
        return idVertex;
    }

    public void setIdVertex(Integer idVertex) {
        this.idVertex = idVertex;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Double getViolenceLevelCovered() {
        return violenceLevelCovered;
    }

    public void setViolenceLevelCovered(Double violenceLevelCovered) {
        this.violenceLevelCovered = violenceLevelCovered;
    }

    public boolean isHasMoved() {
        return hasMoved;
    }

    public void setHasMoved(boolean hasMoved) {
        this.hasMoved = hasMoved;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResourceVertex that = (ResourceVertex) o;
        return Objects.equals(resourceType, that.resourceType) &&
                Objects.equals(idVertex, that.idVertex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resourceType, idVertex);
    }

    @Override
    public String toString() {
        return "ResourceVertex{" +
                "uuid=" + uuid +
                ", resultId=" + resultId +
                ", resourceType=" + resourceType +
                ", idVertex=" + idVertex +
                ", violenceLevelCovered=" + violenceLevelCovered +
                ", hasMoved=" + hasMoved +
                '}';
    }

    public int getTabuIteration() {
        return tabuIteration;
    }

    public void setTabuIteration(int tabuIteration) {
        this.tabuIteration = tabuIteration;
    }
}
