package py.una.fp.ii.tesis.framework.database;

import py.una.fp.ii.tesis.framework.models.Vertex;
import py.una.fp.ii.tesis.framework.utils.FrameworkUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Asigna el vértice correspondiente a cada incidente
 *
 * @author Marco Alvarez
 */
public class AssignVertex {

    private final Database database;
    private final String scheme;

    public AssignVertex(String scheme) {
        this.database = new Database();
        this.scheme = scheme;
    }

    public void run() {
        Connection dbConnection = null;
        Statement statement = null;
        Statement statementVertex = null;
        PreparedStatement preparedStatement = null;


        String selectVertex = "select id, latitude, longitude from  " + scheme + ".vertex";
        String selectLatLonIncidentes = "select latitude, longitude from  " + scheme + ".incidents group by latitude, longitude order by COUNT(id) desc";
        String updateIncidentes = "UPDATE  " + scheme + ".incidents SET vertex_id = ? where latitude = ? and longitude = ?";

        try {
            dbConnection = database.getDBConnection();

            statementVertex = dbConnection.createStatement();
            ResultSet rs = statementVertex.executeQuery(selectVertex);
            List<Vertex> vertexList = new ArrayList<>();
            while (rs.next()) {
                Vertex v = new Vertex(Long.parseLong(rs.getInt("id") + ""), rs.getDouble("latitude"), rs.getDouble("longitude"));
                vertexList.add(v);
            }
            System.out.println("Se cargaron todos los nodos en la lista");
            // execute select SQL stetement
            statement = dbConnection.createStatement();
            rs = statement.executeQuery(selectLatLonIncidentes);
            while (rs.next()) {
                double latitud = rs.getDouble("latitude");
                double longitud = rs.getDouble("longitude");

                Vertex bestVertex = null;
                double distFromBest = 999999999999999999.9;
                for (Vertex v : vertexList) {
                    double distFrom = FrameworkUtils.distFrom(latitud, longitud, v.getLatidud(), v.getLongitud());
                    if (distFrom < distFromBest) {
                        distFromBest = distFrom;
                        bestVertex = v;
                    }
                }

                if (bestVertex != null) {
                    int idVertex = bestVertex.getId().intValue();
//                    System.out.println("Lat: " + latitud + ", Lon: " + longitud + " -> id_vertex: " + idVertex);
                    preparedStatement = dbConnection.prepareStatement(updateIncidentes);
                    preparedStatement.setInt(1, idVertex);
                    preparedStatement.setDouble(2, latitud);
                    preparedStatement.setDouble(3, longitud);
                    preparedStatement.executeUpdate();
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (statementVertex != null) {
                    statementVertex.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
