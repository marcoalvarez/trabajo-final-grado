package py.una.fp.ii.tesis.framework.exec;

import py.una.fp.ii.tesis.framework.algorithms.CoverageHeuristic;

import java.time.LocalDateTime;

/**
 * @author marco
 */
public class MainHeuristic {

    public static void main(String... args) {

        validateInput(args);

        System.out.println("Init process time: " + LocalDateTime.now());
        CoverageHeuristic t = new CoverageHeuristic(args[0]);
        t.run(Integer.parseInt(args[1]));
        System.out.println("End process time: " + LocalDateTime.now());
    }

    private static void validateInput(String... args) throws IllegalArgumentException {
        if (args.length < 2) {
            throw new IllegalArgumentException("Two args needed: scheme, T-Max");
        }
        try {
            Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("T-Max arg is not a number");
        }
    }


}
