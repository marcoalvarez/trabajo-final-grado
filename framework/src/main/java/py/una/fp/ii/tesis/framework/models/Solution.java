package py.una.fp.ii.tesis.framework.models;

import py.una.fp.ii.tesis.framework.database.QueriesForA2A3;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Solution {
    private List<ResourceVertex> resourceVertexList;
    private final String instance;
    private final String type;
    private Long executionTime;
    private final Integer totalEdges;
    private final QueriesForA2A3 queriesForA2A3;
    private Random rand;

    public Solution(List<ResourceVertex> resourceVertexList, String instance, String type, Integer totalEdges, QueriesForA2A3 queriesForA2A3) {
        this.resourceVertexList = resourceVertexList;
        this.instance = instance;
        this.type = type;
        this.totalEdges = totalEdges;
        this.queriesForA2A3 = queriesForA2A3;
        try {
            this.rand = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public double getFcobValue() {
        double leCovered = queriesForA2A3.getLeCovered(loadIdResultList());
        Double leTotal = queriesForA2A3.getLeTotal();
        return leCovered / leTotal;
    }

    public double testFcobValue(ResourceVertex resourceVertex) {
        List<Integer> resultsIdList = loadIdResultList();
        resultsIdList.add(resourceVertex.getResultId());
        return queriesForA2A3.getLeCovered(resultsIdList) / queriesForA2A3.getLeTotal();
    }

    public int testCoverage(ResourceVertex resourceVertex) {
        List<Integer> resultsIdList = loadIdResultList();
        resultsIdList.add(resourceVertex.getResultId());
        return totalEdges - queriesForA2A3.getEdgeCovered(resultsIdList);
    }

    public int getNotCovered() {
        return edgeNotCovered();
    }

    public int edgeNotCovered() {
        return totalEdges - queriesForA2A3.getEdgeCovered(loadIdResultList());
    }

    public List<Integer> loadIdResultList() {
        return resourceVertexList.stream().map(ResourceVertex::getResultId).collect(Collectors.toList());
    }

    public ResourceVertex getRandomResource() {
        ResourceVertex resourceVertex = resourceVertexList.get(this.rand.nextInt(resourceVertexList.size()));
        resourceVertexList.remove(resourceVertex);
        return resourceVertex;
    }

    public ResourceVertex getRandomOfficial() {
        for (ResourceVertex official : resourceVertexList) {
            if (official.getResourceType().equals(3) && !queriesForA2A3.isInFootway(official.getResultId())) {
                return official;
            }
        }
        return null;
    }

    public void addResource(ResourceVertex resourceVertex) {
        resourceVertexList.add(resourceVertex);
    }

    public void removeResource(ResourceVertex resourceVertex) {
        resourceVertexList.remove(resourceVertex);
    }

    public String getInstance() {
        return instance;
    }

    public Solution clone() {
        return new Solution(new ArrayList<>(resourceVertexList), this.instance, this.type, this.totalEdges, this.queriesForA2A3);
    }

    public String getType() {
        return type;
    }

    public Long getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Long executionTime) {
        this.executionTime = executionTime;
    }

    @Override
    public String toString() {
        return "Solution{" +
                "FO=" + getFcobValue() +
                ", resourceVertexList=" + resourceVertexList +
                ", instance='" + instance + '\'' +
                ", type='" + type + '\'' +
                ", executionTime=" + executionTime +
                ", frec=" + getFrecValue() +
                ", fcob=" + getFcobValue() +
                ", edgeNotCovered=" + edgeNotCovered() +
                '}';
    }

    public List<ResourceVertex> getResourceVertexList() {
        return resourceVertexList;
    }

    public double getFrecValue() {
        return resourceVertexList.stream().mapToDouble(ResourceVertex::getViolenceLevelCovered).sum();
    }

    public double testFrecValue(ResourceVertex resourceVertex) {
        double sum = resourceVertexList.stream().mapToDouble(ResourceVertex::getViolenceLevelCovered).sum();
        sum += resourceVertex.getViolenceLevelCovered();
        return sum;
    }
}
