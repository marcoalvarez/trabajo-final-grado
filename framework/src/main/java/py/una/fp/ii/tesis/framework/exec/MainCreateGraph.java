package py.una.fp.ii.tesis.framework.exec;

import py.una.fp.ii.tesis.framework.database.CreateGraphFromCSV;

import java.time.LocalDateTime;

public class MainCreateGraph {

    public static void main(String... args) {
        validateInput();

        CreateGraphFromCSV createGraphFromCSV = new CreateGraphFromCSV(args[0]);
        System.out.println("Inicia proceso: " + LocalDateTime.now());
        try {
            createGraphFromCSV.insertGraph(args[1]);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("Finaliza proceso: " + LocalDateTime.now());
    }

    private static void validateInput(String... args) throws IllegalArgumentException {
        if (args.length < 2) {
            throw new IllegalArgumentException("Two args needed: scheme, pathname");
        }
    }
}
