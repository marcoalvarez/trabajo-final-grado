package py.una.fp.ii.tesis.framework.models;

import java.util.Objects;

/**
 *
 * @author marco
 */
public class Edge {

    private Long id;
    private Vertex origen;
    private Vertex destino;
    private String nombre;
    private String unSentido;
    private String tipoRuta;
    private Double longitud;

    public Edge() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Vertex getOrigen() {
        return origen;
    }

    public void setOrigen(Vertex origen) {
        this.origen = origen;
    }

    public Vertex getDestino() {
        return destino;
    }

    public void setDestino(Vertex destino) {
        this.destino = destino;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUnSentido() {
        return unSentido;
    }

    public void setUnSentido(String unSentido) {
        this.unSentido = unSentido;
    }

    public String getTipoRuta() {
        return tipoRuta;
    }

    public void setTipoRuta(String tipoRuta) {
        this.tipoRuta = tipoRuta;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Edge edge = (Edge) o;
        return Objects.equals(origen, edge.origen) &&
                Objects.equals(destino, edge.destino) &&
                Objects.equals(nombre, edge.nombre);
    }

    @Override
    public int hashCode() {

        return Objects.hash(origen, destino, nombre);
    }

    @Override
    public String toString() {
        return "Edge{" +
                "id=" + id +
                ", origen=" + origen +
                ", destino=" + destino +
                ", nombre='" + nombre + '\'' +
                ", unSentido='" + unSentido + '\'' +
                ", tipoRuta='" + tipoRuta + '\'' +
                ", longitud=" + longitud +
                '}';
    }
}
