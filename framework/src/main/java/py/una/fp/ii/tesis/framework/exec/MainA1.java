package py.una.fp.ii.tesis.framework.exec;

import py.una.fp.ii.tesis.framework.algorithms.RandomA1;

import java.time.LocalDateTime;

public class MainA1 {

    public static void main(String... args) {

        validateInput(args);

        RandomA1 randomA1 = new RandomA1(args[0]);
        System.out.println("Init process time: " + LocalDateTime.now());
        for (int i = 0; i < Integer.parseInt(args[1]); i++) {
            randomA1.calculateRandomSolution(String.valueOf(i));
        }
        System.out.println("End process time: " + LocalDateTime.now());
    }

    private static void validateInput(String... args) throws IllegalArgumentException {
        if(args.length < 2) {
            throw new IllegalArgumentException("Two args needed: scheme, executions");
        }
        try {
            Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("executions arg is not a number");
        }
    }
}
