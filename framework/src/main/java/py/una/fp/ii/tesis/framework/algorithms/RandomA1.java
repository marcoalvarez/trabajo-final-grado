package py.una.fp.ii.tesis.framework.algorithms;

import py.una.fp.ii.tesis.framework.database.Database;
import py.una.fp.ii.tesis.framework.database.QueriesForA1;

import java.util.ArrayList;
import java.util.List;

import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.ID;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.MOTORCYCLE;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.OFFICIAL;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.PATROL;

public class RandomA1 {

    private final QueriesForA1 queriesForA1;
    private final Database database;

    public RandomA1(String scheme) {
        this.database = new Database();
        this.queriesForA1 = new QueriesForA1(scheme, database);
    }

    public void calculateRandomSolution(String instance) {
        long ini = System.currentTimeMillis();
        List<Integer[]> allPoliceDepartments = queriesForA1.getAllPoliceDepartments();
        List<Integer> resultsIds = new ArrayList<>();
        for (Integer[] policeDepartment : allPoliceDepartments) {
            for (int i = 0; i < policeDepartment[PATROL]; i++) {
                Integer vertex = queriesForA1.getVertexByResourceAndDepartment(PATROL, policeDepartment[ID]);
                Integer resultIdByVertexPolice = queriesForA1.getResultIdByVertexPolice(PATROL, vertex);
                resultsIds.add(resultIdByVertexPolice);
            }
            for (int i = 0; i < policeDepartment[MOTORCYCLE]; i++) {
                Integer vertex = queriesForA1.getVertexByResourceAndDepartment(MOTORCYCLE, policeDepartment[ID]);
                Integer resultIdByVertexPolice = queriesForA1.getResultIdByVertexPolice(MOTORCYCLE, vertex);
                resultsIds.add(resultIdByVertexPolice);
            }
            for (int i = 0; i < policeDepartment[OFFICIAL]; i++) {
                Integer vertex = queriesForA1.getVertexByResourceAndDepartment(OFFICIAL, policeDepartment[ID]);
                Integer resultIdByVertexPolice = queriesForA1.getResultIdByVertexPolice(OFFICIAL, vertex);
                resultsIds.add(resultIdByVertexPolice);
            }
        }
        double fcobValue = getFcob(resultsIds);
        double frecValue = getFrec(resultsIds);
        int edgeNotCovered = queriesForA1.getTotalEdge() - queriesForA1.getEdgeCovered(resultsIds);
        long end = System.currentTimeMillis();
        long time = (end - ini) / 1000;
        try {
            queriesForA1.insertSolution(instance, fcobValue, time, resultsIds, frecValue, edgeNotCovered);
//            System.out.println(instance + "\t"+ fcobValue + "\t"+ time + "\t"+ resultsIds + "\t"+ frecValue + "\t"+ edgeNotCovered);
        } catch (Exception e) {
            e.printStackTrace();
        }
        database.closeDBConnection();
    }

    private double getFcob(List<Integer> resultsIds) {
        double leCovered = queriesForA1.getLeCovered(resultsIds);
        Double leTotal = queriesForA1.getLeTotal();
        return leCovered / leTotal;
    }

    private double getFrec(List<Integer> resultsIds) {
        return queriesForA1.getHotSpotFunction(resultsIds);
    }

}
