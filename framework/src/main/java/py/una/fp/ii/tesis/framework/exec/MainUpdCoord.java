package py.una.fp.ii.tesis.framework.exec;

import py.una.fp.ii.tesis.framework.database.UpdateCoordinates;

import java.time.LocalDateTime;

public class MainUpdCoord {

    public static void main(String... args) {

        validateInput(args);

        UpdateCoordinates updateCoordinates = new UpdateCoordinates(args[0]);
        System.out.println("Inicia proceso: " + LocalDateTime.now());
        updateCoordinates.run();
        System.out.println("Finaliza proceso: " + LocalDateTime.now());
    }

    private static void validateInput(String... args) throws IllegalArgumentException {
        if (args.length < 1) {
            throw new IllegalArgumentException("One args needed: scheme");
        }
    }
}
