package py.una.fp.ii.tesis.framework.algorithms;

import py.una.fp.ii.tesis.framework.database.Database;
import py.una.fp.ii.tesis.framework.database.QueriesForCoverage;
import py.una.fp.ii.tesis.framework.models.PoliceResource;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class CoverageHeuristic {

    private final List<Integer> vertexList;
    private final List<PoliceResource> policeResourceList;
    private final QueriesForCoverage queriesForCoverage;

    public CoverageHeuristic(String scheme) {
        this.queriesForCoverage = new QueriesForCoverage(scheme, new Database());
        try {
            vertexList = this.queriesForCoverage.getAllVertex();
            policeResourceList = this.queriesForCoverage.getAllPoliceResources();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new RuntimeException();
        }
    }

    public void run(int tMax) {
        for (PoliceResource policeResource : policeResourceList) {
            heuristicMethod(policeResource, tMax);
        }
    }

    private void heuristicMethod(PoliceResource policeResource, int tMax) {
        System.out.println(policeResource.getName());
        long ini = System.currentTimeMillis();
        ExecutorService service = null;
        try {
            service = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() * 8);
            for (Integer vertexId : vertexList) {
                try {
                    service.execute(new RouteCalculation(vertexId, policeResource, tMax, queriesForCoverage));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } finally {
            if (service != null) {
                service.shutdown();
                try {
                    service.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        long fin = System.currentTimeMillis();
        System.out.println("For resource: " + policeResource.getName());
        System.out.println("Process Time: " + ((fin - ini) / 1000) + " seg.");
        System.out.println("------------------------------------------------------------------------");
    }

}
