package py.una.fp.ii.tesis.framework.exec;

import py.una.fp.ii.tesis.framework.algorithms.TabuSearchA2;
import py.una.fp.ii.tesis.framework.database.Database;
import py.una.fp.ii.tesis.framework.database.QueriesForA2A3;
import py.una.fp.ii.tesis.framework.models.ResourceVertex;
import py.una.fp.ii.tesis.framework.models.Solution;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.MOTORCYCLE;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.OFFICIAL;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.PATROL;

/**
 * @author Marco Alvarez
 */
public class MainA2 {

    private final QueriesForA2A3 queriesForA2A3;
    private final Database database;
    String instance;

    public MainA2(String scheme, String instance) {
        this.database = new Database();
        this.queriesForA2A3 = new QueriesForA2A3(scheme, database);
        this.instance = instance;
    }

    public static void main(String... args) {

        validateInput(args);

        System.out.println("Init process time: " + LocalDateTime.now());
        for (int i = 0; i < Integer.parseInt(args[1]); i++) {
            MainA2 mainA2 = new MainA2(args[0], String.valueOf(i));
            mainA2.runMainA2();
            System.out.println("instancia " + i + " terminado");
        }
        System.out.println("End process time: " + LocalDateTime.now());
    }

    private static void validateInput(String... args) throws IllegalArgumentException {
        if(args.length < 2) {
            throw new IllegalArgumentException("Two args needed: scheme, executions");
        }
        try {
            Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("executions arg is not a number");
        }
    }

    public void runMainA2() {
        long initTime = System.currentTimeMillis();

        List<ResourceVertex> tabuList = new ArrayList<>();
        int patrolsUnits = queriesForA2A3.getResourcePoliceByType(PATROL);
        int motorCyclesUnits = queriesForA2A3.getResourcePoliceByType(MOTORCYCLE);
        int officersUnits = queriesForA2A3.getResourcePoliceByType(OFFICIAL);
        int vertexSize = queriesForA2A3.getVertexSize();
        int tabuSize = (int) (vertexSize * 0.1); // 10% of all vertex
        int neighborSize = tabuSize / 2; //5% of all vertex
        TabuSearchA2 tabuSearchA2 = new TabuSearchA2(tabuList, patrolsUnits, motorCyclesUnits, officersUnits, queriesForA2A3, tabuSize, neighborSize);
        Solution initialSolution = queriesForA2A3.getInitialSolutionA2(instance);
        Solution finalSolution = tabuSearchA2.run(initialSolution);

        long finalTime = System.currentTimeMillis();
        finalSolution.setExecutionTime((finalTime - initTime) / 1000);
        try {
            queriesForA2A3.insertSolution(finalSolution);
//            System.out.println(finalSolution);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        database.closeDBConnection();
    }

}
