package py.una.fp.ii.tesis.framework.database;

import py.una.fp.ii.tesis.framework.models.Edge;
import py.una.fp.ii.tesis.framework.models.Vertex;
import py.una.fp.ii.tesis.framework.utils.FrameworkUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author marco
 */
public class CreateGraphFromCSV {

    private final Database database;
    private final String scheme;

    public CreateGraphFromCSV(String scheme) {
        this.database = new Database();
        this.scheme = scheme;
    }

    public void insertGraph(String pathname) {
        String strLine;
        String idAnterior = "0";
        String x;
        String y;
        String name;
        String highway;
        String oneWay;
        String others;
        Vertex anterior = null;
        List<Vertex> vertexList = new ArrayList<>();
        List<Edge> edgeList = new ArrayList<>();
        try {
            Connection dbConnection = null;
            File file = new File(pathname); //"d:\\geometric-nodes.csv"
            InputStream fileInputStream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream));
            while ((strLine = br.readLine()) != null) {
                String[] split = strLine.split(";");
                String idActual = split[2];
                x = split[1];
                y = split[0];
                name = split[3];
                highway = split[4];
                others = split[5];
                if (others.contains("\"\"oneway\"\"=>\"\"yes\"\"")) {
                    oneWay = "yes";
                } else {
                    oneWay = "no";
                }
                Vertex actual = new Vertex(Double.valueOf(x), Double.valueOf(y));
                if (vertexList.contains(actual)) {
                    int indexOf = vertexList.indexOf(actual);
                    actual = vertexList.get(indexOf);
                } else {
                    vertexList.add(actual);
                }
                if (anterior == null) {
                    anterior = actual;
                    idAnterior = idActual;
                    continue;
                }
                if (idAnterior.equals(idActual)) {
                    Edge aristaVuelta = new Edge();
                    aristaVuelta.setNombre(name);
                    aristaVuelta.setTipoRuta(highway);
                    aristaVuelta.setUnSentido(oneWay);
                    aristaVuelta.setDestino(actual);
                    aristaVuelta.setOrigen(anterior);
                    aristaVuelta.setLongitud(FrameworkUtils.distFrom(anterior.getLatidud(), anterior.getLongitud(), actual.getLatidud(), actual.getLongitud()));

                    Edge aristaIda = new Edge();
                    aristaIda.setNombre(name);
                    aristaIda.setTipoRuta(highway);
                    aristaIda.setUnSentido(oneWay);
                    aristaIda.setDestino(anterior);
                    aristaIda.setOrigen(actual);
                    aristaIda.setLongitud(FrameworkUtils.distFrom(actual.getLatidud(), actual.getLongitud(), anterior.getLatidud(), anterior.getLongitud()));
                    edgeList.add(aristaIda);
                    edgeList.add(aristaVuelta);
                }
                anterior = actual;
                idAnterior = idActual;
            }
            br.close();

            PreparedStatement preparedStatementInsert = null;
            String insertVertex = "INSERT INTO  " + scheme + ".vertex(latitude, longitude) VALUES (?, ?)";
            String insertEdge = "INSERT INTO  " + scheme + ".edge(street_name, oneway, highway, origin_id, destination_id, length) VALUES (?, ?, ?, ?, ?, ?)";
            try {
                dbConnection = database.getDBConnection();
                dbConnection.setAutoCommit(false);
                for (Vertex vert : vertexList) {
                    int i = 0;
                    preparedStatementInsert = dbConnection.prepareStatement(insertVertex, Statement.RETURN_GENERATED_KEYS);
                    preparedStatementInsert.setDouble(++i, vert.getLatidud());
                    preparedStatementInsert.setDouble(++i, vert.getLongitud());
                    int affectedRows = preparedStatementInsert.executeUpdate();
                    if (affectedRows == 0) {
                        throw new SQLException("Error al crear: " + vert);
                    }

                    try (ResultSet generatedKeys = preparedStatementInsert.getGeneratedKeys()) {
                        if (generatedKeys.next()) {
                            vert.setId(generatedKeys.getLong(1));
                        } else {
                            throw new SQLException("Creating vertex failed, no ID obtained." + vert);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                for (Edge edge : edgeList) {
                    int i = 0;
                    preparedStatementInsert = dbConnection.prepareStatement(insertEdge);
                    preparedStatementInsert.setString(++i, edge.getNombre());
                    preparedStatementInsert.setString(++i, edge.getUnSentido());
                    preparedStatementInsert.setString(++i, edge.getTipoRuta());
                    preparedStatementInsert.setInt(++i, edge.getOrigen().getId().intValue());
                    preparedStatementInsert.setInt(++i, edge.getDestino().getId().intValue());
                    preparedStatementInsert.setDouble(++i, edge.getLongitud());
                    int affectedRows = preparedStatementInsert.executeUpdate();
                    if (affectedRows == 0) {
                        throw new SQLException("Error al crear: " + edge);
                    }
                }

                dbConnection.commit();
                System.out.println("Done!");
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                dbConnection.rollback();
            } finally {
                if (preparedStatementInsert != null) {
                    preparedStatementInsert.close();
                }
                if (dbConnection != null) {
                    dbConnection.close();
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
