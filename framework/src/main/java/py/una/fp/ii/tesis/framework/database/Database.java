package py.una.fp.ii.tesis.framework.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.DB_CONNECTION;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.DB_PASSWORD;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.DB_USER;

/**
 *
 * @author marco
 */
public class Database {

    public Database() {
    }

    private static Connection dbConnection = null;

    public void openConnection() {
        if (dbConnection == null) {
            try {
                dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public Connection getDBConnection() {
        if (dbConnection == null) {
            openConnection();
        }
        return dbConnection;
    }

    public void closeDBConnection() {
        if (dbConnection != null) {
            try {
                dbConnection.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            } finally {
                dbConnection = null;
            }
        }
    }
}
