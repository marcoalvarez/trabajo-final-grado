package py.una.fp.ii.tesis.framework.database;

import py.una.fp.ii.tesis.framework.models.CycleResult;
import py.una.fp.ii.tesis.framework.models.PoliceResource;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.MOTORCYCLE;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.OFFICIAL;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.PATROL;

/**
 * @author marco
 */
public class QueriesForCoverage {

    private final String scheme;
    private final Database database;

    public QueriesForCoverage(String scheme, Database database) {
        this.scheme = scheme;
        this.database = database;
    }

    public List<Integer> getAllVertex() {
        List<Integer> result = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        String selectSQL = "SELECT id FROM " + scheme + ".vertex";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result.add(rs.getInt("id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public List<PoliceResource> getAllPoliceResources() {
        List<PoliceResource> result = new ArrayList<>();
        result.add(new PoliceResource("Official", OFFICIAL));
        result.add(new PoliceResource("Patrol", PATROL));
        result.add(new PoliceResource("Motorcycle", MOTORCYCLE));
        return result;
    }

    public List<Object[]> getEdgeListByVertex(Integer originId) {
        List<Object[]> resultList = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        String selectSQL = "SELECT id, destination_id, patrol_time, motorcycle_time, official_time FROM " + scheme + ".edge where origin_id = ?";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, originId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Object[] result = new Object[]{rs.getInt("id"), rs.getInt("destination_id"), rs.getDouble("patrol_time"), rs.getDouble("motorcycle_time"), rs.getDouble("official_time")};
                resultList.add(result);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return resultList;
    }

    public Integer getEdgeByOriginIdDestinationId(Integer originId, Integer destinationId) {
        PreparedStatement preparedStatement = null;
        String selectSQL = "SELECT id FROM " + scheme + ".edge where origin_id = ? and destination_id = ?";
        Integer result = null;
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, originId);
            preparedStatement.setInt(2, destinationId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public double getViolenceLevel(List<Integer> edgeList) {
        PreparedStatement preparedStatement = null;
        String selectSQL = "SELECT sum(violence_level) as violence FROM " + scheme + ".edge where id = ANY(?)";
        Double result = 0.0;
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            Array array = dbConnection.createArrayOf("int4", edgeList.toArray());
            preparedStatement.setArray(1, array);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getDouble("violence");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public int insertResults(CycleResult cycleResult) throws Exception {
        PreparedStatement preparedStatement = null;
        String selectSQL = " INSERT INTO " + scheme + ".results(violence_level, vertex_id, police_resource_type) VALUES (?, ?, ?);";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setDouble(1, cycleResult.getViolenceLevel());
            preparedStatement.setInt(2, cycleResult.getSelectedVertex());
            preparedStatement.setInt(3, cycleResult.getPoliceResource().getType());
            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating CycleResult failed, no rows affected.");
            }

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            } else {
                throw new SQLException("Creating CycleResult failed, no ID obtained.");
            }
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void insertResultsVertex(Integer resultId, List<Integer> vertexList) {
        PreparedStatement preparedStatement = null;
        String selectSQL = " INSERT INTO " + scheme + ".results_vertex( "
                + "            result_id, vertex_id) "
                + "    VALUES (?, ?)";
        try {
            Connection dbConnection = database.getDBConnection();
            for (Integer vertexId : vertexList) {
                preparedStatement = dbConnection.prepareStatement(selectSQL);
                preparedStatement.setInt(1, resultId);
                preparedStatement.setInt(2, vertexId);
                preparedStatement.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public void insertResultsEdge(Integer resultId, List<Integer> edgeList) {

        PreparedStatement preparedStatement = null;
        String selectSQL = " INSERT INTO " + scheme + ".results_edge( "
                + "             result_id, edge_id) "
                + "    VALUES (?, ?)";
        try {
            Connection dbConnection = database.getDBConnection();
            for (Integer edge : edgeList) {
                preparedStatement = dbConnection.prepareStatement(selectSQL);
                preparedStatement.setInt(1, resultId);
                preparedStatement.setInt(2, edge);
                preparedStatement.executeUpdate();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

}
