package py.una.fp.ii.tesis.framework.database;

import py.una.fp.ii.tesis.framework.models.ResourceVertex;
import py.una.fp.ii.tesis.framework.models.Solution;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.MOTORCYCLE;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.OFFICIAL;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.PATROL;

/**
 * @author marco
 */
public class QueriesForA2A3 {

    private String scheme;
    private Database database;
    private Double leTotal = null;

    public QueriesForA2A3(String scheme, Database database) {
        this.scheme = scheme;
        this.database = database;
    }

    public List<ResourceVertex> getNeighborsByResourceVertex(ResourceVertex resourceVertex, int limit) {
        List<ResourceVertex> result = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        String selectSQL = "select id, vertex_id, violence_level from " + scheme + ".results r where r.vertex_id in (" +
                "select rv.vertex_id from " + scheme + ".results_vertex rv where rv.result_id = ? and rv.vertex_id <> ? " +
                "ORDER BY RANDOM() LIMIT ?) and police_resource_type = ?";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            int i = 0;
            preparedStatement.setInt(++i, resourceVertex.getResultId());
            preparedStatement.setInt(++i, resourceVertex.getIdVertex());
            preparedStatement.setInt(++i, limit);
            preparedStatement.setInt(++i, resourceVertex.getResourceType());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result.add(new ResourceVertex(UUID.randomUUID(),
                        rs.getInt("id"),
                        resourceVertex.getResourceType(),
                        rs.getInt("vertex_id"),
                        rs.getDouble("violence_level")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    private Double executeQueryDouble(String selectSQL, List<Integer> resultIdList) {
        Double result = null;
        PreparedStatement preparedStatement = null;
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            Array array = dbConnection.createArrayOf("int4", resultIdList.toArray());
            preparedStatement.setArray(1, array);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getDouble("violence");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public Solution getInitialSolutionA2(String instance) {
        Solution result = null;
        List<ResourceVertex> resourceVertexList = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        String selectSQL = "select r.id, r.vertex_id, r.violence_level from " + scheme + ".results r " +
                "where r.police_resource_type = ? and r.violence_level > 0 order by RANDOM() desc LIMIT ?";
        try {
            Connection dbConnection = database.getDBConnection();

            //PATROLS
            int i = 0;
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(++i, PATROL);
            preparedStatement.setInt(++i, getResourcePoliceByType(PATROL));
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                resourceVertexList.add(new ResourceVertex(
                        UUID.randomUUID(),
                        rs.getInt("id"),
                        PATROL,
                        rs.getInt("vertex_id"),
                        rs.getDouble("violence_level")));
            }

            //MOTORCYCLES
            i = 0;
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(++i, MOTORCYCLE);
            preparedStatement.setInt(++i, getResourcePoliceByType(MOTORCYCLE));
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                resourceVertexList.add(new ResourceVertex(
                        UUID.randomUUID(),
                        rs.getInt("id"),
                        MOTORCYCLE,
                        rs.getInt("vertex_id"),
                        rs.getDouble("violence_level")));
            }

            //OFFICIALS
            i = 0;
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(++i, OFFICIAL);
            preparedStatement.setInt(++i, getResourcePoliceByType(OFFICIAL));
            rs = preparedStatement.executeQuery();
            while (rs.next()) {
                resourceVertexList.add(new ResourceVertex(
                        UUID.randomUUID(),
                        rs.getInt("id"),
                        OFFICIAL,
                        rs.getInt("vertex_id"),
                        rs.getDouble("violence_level")));
            }

            result = new Solution(resourceVertexList, instance, "A2", getCountEdge(), this);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public Solution getInitialSolutionHotSpotA3(String instance) {
        Solution result = null;
        List<ResourceVertex> resourceVertexList = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        List<Integer> originIds = new ArrayList<>();

        String selectHotSpot = "select distinct(l.origin_id) from (SELECT origin_id from " + scheme + ".edge " +
                "where violence_level = 1 order by random() desc) l limit ?";

        String selectSQL = "select r.id, r.vertex_id, r.violence_level from " + scheme + ".results r " +
                "where r.police_resource_type = ? and r.vertex_id = ?";
        try {
            Connection dbConnection = database.getDBConnection();
            int i = 0;

            preparedStatement = dbConnection.prepareStatement(selectHotSpot);
            preparedStatement.setInt(++i, getAllResourceAmount());
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                originIds.add(rs.getInt("origin_id"));
            }

            //PATROLS
            int patrol = getResourcePoliceByType(PATROL);
            for (int j = 0; j < patrol; j++) {
                i = 0;
                preparedStatement = dbConnection.prepareStatement(selectSQL);
                preparedStatement.setInt(++i, PATROL);
                preparedStatement.setInt(++i, originIds.get(j));
                rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    resourceVertexList.add(new ResourceVertex(
                            UUID.randomUUID(),
                            rs.getInt("id"),
                            PATROL,
                            rs.getInt("vertex_id"),
                            rs.getDouble("violence_level")));
                }
            }

            //MOTORCYCLES
            int motorcicle = getResourcePoliceByType(MOTORCYCLE);
            for (int j = patrol; j < motorcicle + patrol; j++) {
                i = 0;
                preparedStatement = dbConnection.prepareStatement(selectSQL);
                preparedStatement.setInt(++i, MOTORCYCLE);
                preparedStatement.setInt(++i, originIds.get(j));
                rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    resourceVertexList.add(new ResourceVertex(
                            UUID.randomUUID(),
                            rs.getInt("id"),
                            MOTORCYCLE,
                            rs.getInt("vertex_id"),
                            rs.getDouble("violence_level")));
                }
            }

            //OFFICIALS
            int police = getResourcePoliceByType(OFFICIAL);
            for (int j = motorcicle + patrol; j < police + motorcicle + patrol; j++) {
                i = 0;
                preparedStatement = dbConnection.prepareStatement(selectSQL);
                preparedStatement.setInt(++i, OFFICIAL);
                preparedStatement.setInt(++i, originIds.get(j));
                rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    resourceVertexList.add(new ResourceVertex(
                            UUID.randomUUID(),
                            rs.getInt("id"),
                            OFFICIAL,
                            rs.getInt("vertex_id"),
                            rs.getDouble("violence_level")));
                }
            }

            result = new Solution(resourceVertexList, instance, "A3", getCountEdge(), this);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }


    public Integer getVertexNotCovered(List<Integer> resultsIdList) {
        Integer result = null;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select e.origin_id from (select distinct re.edge_id from " + scheme + ".results_edge re   " +
                "where re.result_id = ANY(?)) " +
                "r1 " +
                "right join " +
                "" + scheme + ".edge e on r1.edge_id=e.id where r1.edge_id is null " +
                "order by random() " +
                "limit 1";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            Array array = dbConnection.createArrayOf("int4", resultsIdList.toArray());
            preparedStatement.setArray(1, array);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt("origin_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public Object[] getResultIdByVertexPolice(Integer resourceType, Integer vertexNotCovered) {
        Object[] result = new Object[2];
        PreparedStatement preparedStatement = null;
        String selectSQL = "select r.id, r.violence_level from " + scheme + ".results r " +
                "where r.police_resource_type = ? AND r.vertex_id = ? ";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, resourceType);
            preparedStatement.setInt(2, vertexNotCovered);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result[0] = rs.getInt("id");
                result[1] = rs.getDouble("violence_level");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public Integer getResultIdByIntersection(Integer official, List<Integer> resultsIds) {
        Integer result = null;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select " + scheme + ".buscar_mayor_interseccion(?, ?);";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, official);
            Array array = dbConnection.createArrayOf("int4", resultsIds.toArray());
            preparedStatement.setArray(2, array);
            ResultSet rs = preparedStatement.executeQuery();
            Object object = null;
            while (rs.next()) {
                object = rs.getObject(1);
            }
            if (object != null) {
                result = (Integer) object;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public boolean isInFootway(Integer resultId) {
        Integer result = 0;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select count(e.* ) " +
                "from " + scheme + ".edge e  " +
                "join " + scheme + ".vertex v on e.origin_id = v.id " +
                "join " + scheme + ".results r on r.vertex_id = v.id " +
                "where r.id = ? and e.patrol_time = -1";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, resultId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        // result = 0 : ninguna de las aristas del nodo es un footway
        // result > 0 : al menos una de las aristas es footway
        return !result.equals(0);
    }

    public void insertSolution(Solution solution) throws Exception {
        PreparedStatement preparedStatement = null;
        String selectSQL = " INSERT INTO " + scheme + ".solutions(instance, solution_type, f_cob, execution_time, " +
                "f_rec, edge_not_covered) VALUES (?, ?, ?, ?, ?, ?);";
        try {
            int i = 0;
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(++i, solution.getInstance());
            preparedStatement.setString(++i, solution.getType());
            preparedStatement.setDouble(++i, solution.getFcobValue());
            preparedStatement.setDouble(++i, solution.getExecutionTime());
            preparedStatement.setDouble(++i, solution.getFrecValue());
            preparedStatement.setInt(++i, solution.edgeNotCovered());
            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating Solution failed, no rows affected.");
            }

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                insertSolutionResult(solution.getResourceVertexList(), generatedKeys.getInt(1));
            } else {
                throw new SQLException("Creating Solution failed, no ID obtained.");
            }
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void insertSolutionResult(List<ResourceVertex> resourceVertexList, Integer idSolution) {
        PreparedStatement preparedStatement = null;
        String selectSQL = " INSERT INTO " + scheme + ".solutions_results(solution_id, result_id) VALUES (?, ?);";
        try {
            Connection dbConnection = database.getDBConnection();
            for (ResourceVertex rv : resourceVertexList) {
                preparedStatement = dbConnection.prepareStatement(selectSQL);
                preparedStatement.setInt(1, idSolution);
                preparedStatement.setInt(2, rv.getResultId());
                int affectedRows = preparedStatement.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Creating Solution failed, no rows affected.");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    public int getResourcePoliceByType(Integer policeType) {
        int result = 0;
        PreparedStatement preparedStatement = null;
        String police = "";
        switch (policeType) {
            case PATROL:
                police = "total_patrols";
                break;
            case MOTORCYCLE:
                police = "total_motorcycles";
                break;
            case OFFICIAL:
                police = "total_officials";
                break;
        }
        String selectSQL = "select sum(" + police + ") from " + scheme + ".comisarias";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    private int getAllResourceAmount() {
        int result = 0;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select sum(total_patrols)+sum(total_motorcycles)+sum(total_officials) as total from "
                + scheme + ".comisarias";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt("total");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }


    private int getCountEdge() {
        int result = 0;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select count(*) from " + scheme + ".edge";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public int getVertexSize() {
        int result = 0;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select count(id) from " + scheme + ".vertex";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    /**
     * Obtiene la suma del nivel de violencia asociado a esos resultados
     *
     * @param resultIdList lista de IDs de resultado
     * @return valor de nivel de violencia cubierto
     */
    public Integer getEdgeCovered(List<Integer> resultIdList) {
        Integer result = null;
        String selectSQL = "select count(distinct re.edge_id) as violence from " + scheme + ".results_edge re join "
                + scheme + ".edge e on re.edge_id = e.id where re.result_id = ANY(?)";
        PreparedStatement preparedStatement = null;
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            Array array = dbConnection.createArrayOf("int4", resultIdList.toArray());
            preparedStatement.setArray(1, array);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt("violence");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public Double getLeTotal() {
        if (leTotal == null) {
            leTotal = calculateLeTotal();
        }
        return leTotal;
    }

    private Double calculateLeTotal() {
        double result = 0.0;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select sum(violence_level) as violence_level from " + scheme + ".edge;";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getDouble("violence_level");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    /**
     * Obtiene la suma del nivel de violencia asociado a esos resultados
     *
     * @param resultIdList lista de IDs de resultado
     * @return valor de nivel de violencia cubierto
     */
    public double getLeCovered(List<Integer> resultIdList) {
        String selectSQL = "select sum(violence_level) as violence from (select distinct re.edge_id, e.violence_level" +
                " from " + scheme + ".results_edge re join " + scheme + ".edge e on re.edge_id = e.id " +
                "where re.result_id = ANY(?)) as temp";
        return executeQueryDouble(selectSQL, resultIdList);
    }

    public double[] getLatitudeLongitude(Integer vertexId) {
        double[] result = new double[2];
        PreparedStatement preparedStatement = null;
        String selectSQL = "select v.latitude, v.longitude from  "+ scheme + ".vertex v where v.id = ? ";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, vertexId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result[0] = rs.getDouble("latitude");
                result[1] = rs.getDouble("longitude");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }
}
