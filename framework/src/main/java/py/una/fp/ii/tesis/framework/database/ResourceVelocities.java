package py.una.fp.ii.tesis.framework.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ResourceVelocities {

    private final Database database;
    private final String scheme;

    public ResourceVelocities(String scheme) {
        this.database = new Database();
        this.scheme = scheme;
    }

    public void run(double patrolVelocity, double motorcycleVelocity, double officialVelocity) {
        Connection dbConnection = null;
        PreparedStatement preparedStatement = null;

        String normalVelocitiesUpdate = "UPDATE " + scheme + ".edge " +
                " SET  " +
                " patrol_time=length/?,  " +
                " motorcycle_time=length/?,  " +
                " official_time=length/?";

        String oppositeVelocitiesUpdate = "UPDATE " + scheme + ".edge " +
                " SET  " +
                " patrol_time=(length/?) * 1.5,  " +
                " motorcycle_time=(length/?) * 1.5  " +
                "WHERE " +
                " oneway = 'yes' and ( id % 2 ) = 0 ";

        String footVelocitiesUpdate = "UPDATE " + scheme + ".edge " +
                " SET  " +
                " patrol_time=-1.0,  " +
                " motorcycle_time=-1.0  " +
                "WHERE " +
                " highway = 'path'";

        try {
            dbConnection = database.getDBConnection();
            int i = 0;
            preparedStatement = dbConnection.prepareStatement(normalVelocitiesUpdate);
            preparedStatement.setDouble(++i, patrolVelocity);
            preparedStatement.setDouble(++i, motorcycleVelocity);
            preparedStatement.setDouble(++i, officialVelocity);
            preparedStatement.executeUpdate();

            i = 0;
            preparedStatement = dbConnection.prepareStatement(oppositeVelocitiesUpdate);
            preparedStatement.setDouble(++i, patrolVelocity);
            preparedStatement.setDouble(++i, motorcycleVelocity);
            preparedStatement.executeUpdate();

            preparedStatement = dbConnection.prepareStatement(footVelocitiesUpdate);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
