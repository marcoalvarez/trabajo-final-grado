package py.una.fp.ii.tesis.framework.database;

import py.una.fp.ii.tesis.framework.utils.CoordinateConversion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Marco Alvarez
 */
public class UpdateCoordinates {

    private final Database database;
    private final String scheme;

    public UpdateCoordinates(String scheme) {
        this.database = new Database();
        this.scheme = scheme;
    }

    public void run() {
        Connection dbConnection = null;
        Statement statement = null;
        PreparedStatement preparedStatement = null;

        String selectTableSQL = "select latitude_utm, longitude_utm from " + scheme + ".incidents group by latitude_utm, longitude_utm order by COUNT(id) desc";
        String updateTableSQL = "UPDATE " + scheme + ".incidents SET latitude=?, longitude=? where latitude_utm = ? and longitude_utm = ?";

        try {
            dbConnection = database.getDBConnection();
            statement = dbConnection.createStatement();
            CoordinateConversion coordinateConversion = new CoordinateConversion();
            // execute select SQL stetement
            ResultSet rs = statement.executeQuery(selectTableSQL);
            while (rs.next()) {
                String latitudutm = rs.getString("latitude_utm");
                String longitudutm = rs.getString("longitude_utm");

                double[] doubles = coordinateConversion.utm2LatLon("21 E " + getFixedDoubleString(latitudutm) + " " + getFixedDoubleString(longitudutm));

                preparedStatement = dbConnection.prepareStatement(updateTableSQL);
                preparedStatement.setDouble(1, doubles[0]);
                preparedStatement.setDouble(2, doubles[1]);
                preparedStatement.setString(3, latitudutm);
                preparedStatement.setString(4, longitudutm);

                // execute update SQL stetement
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {

            System.out.println(e.getMessage());

        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (dbConnection != null) {
                    dbConnection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    private String getFixedDoubleString(String value) {
        String[] split = value.split("\\.");
        int length = split.length;
        int i = 0;
        StringBuilder result = new StringBuilder();
        for (int j = 1; j < length; i++, j++) {
            result.append(split[i]);
        }
        result.append(".");
        result.append(split[i]);
        return result.toString();
    }
}
