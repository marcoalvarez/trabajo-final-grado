package py.una.fp.ii.tesis.framework.exec;

import py.una.fp.ii.tesis.framework.database.AssignVertex;

import java.time.LocalDateTime;

public class MainAssignVertex {

    public static void main(String... args) {

        validateInput(args);

        AssignVertex assignVertex = new AssignVertex(args[0]);
        System.out.println("Inicia proceso: " + LocalDateTime.now());
        assignVertex.run();
        System.out.println("Finaliza proceso: " + LocalDateTime.now());
    }

    private static void validateInput(String... args) throws IllegalArgumentException {
        if (args.length < 1) {
            throw new IllegalArgumentException("One args needed: scheme");
        }
    }
}
