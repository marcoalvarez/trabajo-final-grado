package py.una.fp.ii.tesis.framework.utils;

public class FrameworkConstant {

    public static final String DB_CONNECTION = "jdbc:postgresql://127.0.0.1:5432/tesis";
    public static final String DB_USER = "tesis";
    public static final String DB_PASSWORD = "alvarezpenayo";

    public static final int ID = 0;
    public static final int PATROL = 1;
    public static final int MOTORCYCLE = 2;
    public static final int OFFICIAL = 3;

}
