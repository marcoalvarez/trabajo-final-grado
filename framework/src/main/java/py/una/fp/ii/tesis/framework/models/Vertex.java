package py.una.fp.ii.tesis.framework.models;

import java.util.Objects;

/**
 *
 * @author marco
 */
public class Vertex {

    private Long id;
    private Double latidud;
    private Double longitud;

    public Vertex(Double latidud, Double longitud) {
        this.latidud = latidud;
        this.longitud = longitud;
    }

    public Vertex(Long id, Double latidud, Double longitud) {
        this.id = id;
        this.latidud = latidud;
        this.longitud = longitud;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getLatidud() {
        return latidud;
    }

    public void setLatidud(Double latidud) {
        this.latidud = latidud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex vertex = (Vertex) o;
        return Objects.equals(latidud, vertex.latidud) &&
                Objects.equals(longitud, vertex.longitud);
    }

    @Override
    public int hashCode() {

        return Objects.hash(latidud, longitud);
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "id=" + id +
                ", latidud='" + latidud + '\'' +
                ", longitud='" + longitud + '\'' +
                '}';
    }
}
