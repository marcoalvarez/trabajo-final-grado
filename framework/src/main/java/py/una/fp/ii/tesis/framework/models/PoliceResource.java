package py.una.fp.ii.tesis.framework.models;

/**
 * @author Marco Alvarez
 */
public class PoliceResource {
    private String name;
    private int type;

    public PoliceResource(String name, int type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PoliceResource{" +
                "name=" + name +
                ", type=" + type +
                '}';
    }
}

