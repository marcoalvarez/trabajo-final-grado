package py.una.fp.ii.tesis.framework.algorithms;

import py.una.fp.ii.tesis.framework.utils.FrameworkConstant;
import py.una.fp.ii.tesis.framework.database.QueriesForA2A3;
import py.una.fp.ii.tesis.framework.models.ResourceVertex;
import py.una.fp.ii.tesis.framework.models.Solution;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TabuSearchA2 {

    private List<ResourceVertex> tabuList;
    private final int MAX_ITERATION;
    private final int U;
    private int globalIteration;
    private final QueriesForA2A3 queriesForA2A3;
    private final int NEIGHBOR_SIZE;
    private final int TABU_CYCLE;

    public TabuSearchA2(List<ResourceVertex> tabuList, int patrolsUnits, int motorCyclesUnits, int officersUnits,
                        QueriesForA2A3 queriesForA2A3, int tabuSize, int neighborSize) {

        this.tabuList = tabuList;
        U = patrolsUnits + motorCyclesUnits + officersUnits;
        MAX_ITERATION = U * 4;
        this.queriesForA2A3 = queriesForA2A3;
        this.NEIGHBOR_SIZE = neighborSize;
        this.TABU_CYCLE = tabuSize;
    }

    public Solution run(Solution initialSolution) {
        int currentIteration = 0;
        globalIteration = 0;

        Solution bestSolution = initialSolution.clone();
        Solution currentSolution = initialSolution.clone();

        int notCoveredEdge = bestSolution.getNotCovered();
        int iteracionSinMejora = 0;
        boolean cambioFcob = false;
        while (currentIteration < MAX_ITERATION) {
            ResourceVertex resource = currentSolution.getRandomResource();
            if (resource == null) {
                break;
            }
            ResourceVertex bestNeighbor = null;
            currentIteration++;

            List<ResourceVertex> neighborsList = getNeighbors(resource, NEIGHBOR_SIZE);
            for (ResourceVertex neighbor : neighborsList) {
                if (!currentSolution.getResourceVertexList().contains(neighbor) && !tabuList.contains(neighbor)) {
                    int testFcobValue = currentSolution.testCoverage(neighbor);
                    if (notCoveredEdge != 0 && testFcobValue <= notCoveredEdge) {
                        cambioFcob = testFcobValue != notCoveredEdge;
                        bestNeighbor = neighbor;
                        notCoveredEdge = testFcobValue;
                        neighbor.setTabuIteration(globalIteration + TABU_CYCLE);
                        tabuList.add(neighbor);
                    }
                }
            }
            if (bestNeighbor != null) {
                currentSolution.addResource(bestNeighbor);
                bestSolution = currentSolution.clone();
                currentIteration = 0;
                if (!cambioFcob) {
                    iteracionSinMejora++;
                } else {
                    iteracionSinMejora = 0;
                }
            } else {
                currentSolution.addResource(resource);
            }
            //Intensification & Diversificación
            if (iteracionSinMejora % U == 0 && iteracionSinMejora > 0) {
                try {
                    doIntensificationDiversification(bestSolution);
                    currentSolution = bestSolution.clone();
                    notCoveredEdge = bestSolution.getNotCovered();
                } catch (NotMoreSolutionException ex) {
                    break;
                }
            }
            // Removemos los recursos policiales que estaban tabu y han cumplido ciclo
            List<ResourceVertex> collect = tabuList.stream()
                    .filter(resourceVertex -> resourceVertex.getTabuIteration() == globalIteration)
                    .collect(Collectors.toList());

            tabuList.removeAll(collect);
            globalIteration++;
        }
        return bestSolution;
    }

    private void doIntensificationDiversification(Solution solution) throws NotMoreSolutionException {

        int NEIGHBOR_SIZE_INTENSIFICATION = 15;
        int VIOLENCE_LEVEL_POS = 1;

        // oficial movible
        ResourceVertex official = solution.getRandomOfficial();
        if (official != null) {
            // guardamos su nodo actual (que va ser el previo)
            Integer previousVertex = official.getIdVertex();

            // traemos todos los resultados de los demas recursos
            List<Integer> resultsIds = solution.getResourceVertexList().stream()
                    .filter(resourceVertex -> !resourceVertex.getUuid().equals(official.getUuid()))
                    .map(ResourceVertex::getResultId)
                    .collect(Collectors.toList());

            // obtenemos algun nodo que NO este cubierto por todos los recursos (sin contar con el oficial)
            Integer vertexNotCovered = queriesForA2A3.getVertexNotCovered(resultsIds);
            if (vertexNotCovered == null) {
                // Todos los nodos ya se encuentran cubiertos
                throw new NotMoreSolutionException();
            }

            // asignamos el nodo no cubierto al oficial, obtenemos su id de resultado y marcamos que ya se movio
            official.setIdVertex(vertexNotCovered);
            Object[] resultIdByVertexPolice = queriesForA2A3.getResultIdByVertexPolice(official.getResourceType(),
                    vertexNotCovered);
            official.setResultId((Integer) resultIdByVertexPolice[FrameworkConstant.ID]);
            official.setViolenceLevelCovered((Double) resultIdByVertexPolice[VIOLENCE_LEVEL_POS]);
            official.setHasMoved(true);


            resultsIds.clear();
            // obtenemos los id de resultados de las patrulleras y motocicletas de la solucion (no se consideran los
            // demas oficiales del resultado)
            resultsIds = solution.getResourceVertexList().stream()
                    .filter(resourceVertex -> resourceVertex.getResourceType() != 3)
                    .map(ResourceVertex::getResultId)
                    .collect(Collectors.toList());

            // obtener recurso (patrullera o motocicleta) con mayor interseccion
            Integer resultId = queriesForA2A3.getResultIdByIntersection(official.getResultId(), resultsIds);
            if (resultId == null) {
                // El oficial en su nueva posicion no tiene intereseccion es con alguna patrullera o moto
                return;
            }

            // verificamos que no exista ningun oficial en ese lugar
            Optional<ResourceVertex> optionalResourceVertex = solution.getResourceVertexList().stream()
                    .filter(resourceVertex -> resourceVertex.getResultId().equals(resultId))
                    .findFirst();
            ResourceVertex exchangeResource = optionalResourceVertex.orElse(null);
            // si exchangeResource == null el recurso ya fue movido previamente
            if (exchangeResource != null) {
                // quitamos al recurso a intercambiar
                solution.removeResource(exchangeResource);
                // le asignamos el lugar del oficial cambiado
                exchangeResource.setIdVertex(previousVertex);
                // buscamos el id de resultado correspondiente a ese nodo para ese tipo de recurso
                Object[] resultIdByVertexPolice1 = queriesForA2A3.getResultIdByVertexPolice(
                        exchangeResource.getResourceType(),
                        previousVertex);
                exchangeResource.setResultId((Integer) resultIdByVertexPolice1[FrameworkConstant.ID]);
                exchangeResource.setViolenceLevelCovered((Double) resultIdByVertexPolice1[VIOLENCE_LEVEL_POS]);
            } else {
                //El recurso ya fue movido previamente
                return;
            }

            // iniciamos la busqueda local
            List<ResourceVertex> neighborsList = getNeighbors(exchangeResource, NEIGHBOR_SIZE_INTENSIFICATION);
            ResourceVertex bestNeighbor = null;
            double bestFcobSoFar = solution.testFcobValue(exchangeResource);
            for (ResourceVertex neighbor : neighborsList) {
                if (!solution.getResourceVertexList().contains(neighbor)) {
                    double testFcob = solution.testFcobValue(neighbor);
                    if (testFcob > bestFcobSoFar) {
                        bestNeighbor = neighbor;
                        bestFcobSoFar = testFcob;
                    }
                }
            }
            if (bestNeighbor != null) {
                bestNeighbor.setHasMoved(true);
                solution.getResourceVertexList().add(bestNeighbor);
            } else {
                exchangeResource.setHasMoved(true);
                solution.getResourceVertexList().add(exchangeResource);
            }
        } else {
            // Ya no quedan oficiales por reubicar
            throw new NotMoreSolutionException();
        }
    }

    private List<ResourceVertex> getNeighbors(ResourceVertex resourceVertex, int limit) {
        return queriesForA2A3.getNeighborsByResourceVertex(resourceVertex, limit);
    }

    private static class NotMoreSolutionException extends Exception {
        NotMoreSolutionException() {
        }
    }
}
