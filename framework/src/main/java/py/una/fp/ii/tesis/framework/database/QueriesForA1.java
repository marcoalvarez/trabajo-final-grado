package py.una.fp.ii.tesis.framework.database;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.ID;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.MOTORCYCLE;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.OFFICIAL;
import static py.una.fp.ii.tesis.framework.utils.FrameworkConstant.PATROL;

/**
 * Clase con las consultas a la base de datos correspodientes al algoritmo A1.
 *
 * @author Marco Alvarez
 */
public class QueriesForA1 {

    private final String scheme;
    private final Database database;
    private Double leTotal = null;

    public QueriesForA1(String scheme, Database database) {
        this.scheme = scheme;
        this.database = database;
    }

    /**
     * Obtiene las comisarias identificadas con su ID y la cantiadad de recursos que posee por tipo.
     *
     * @return Lista de comisarias con sus cantidades de recursos
     */
    public List<Integer[]> getAllPoliceDepartments() {
        List<Integer[]> result = new ArrayList<>();
        PreparedStatement preparedStatement = null;
        String selectSQL = "SELECT id, total_patrols, total_motorcycles, total_officials FROM " + scheme + ".comisarias";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                Integer[] r = new Integer[4];
                r[ID] = rs.getInt("id");
                r[PATROL] = rs.getInt("total_patrols");
                r[MOTORCYCLE] = rs.getInt("total_motorcycles");
                r[OFFICIAL] = rs.getInt("total_officials");
                result.add(r);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    /**
     * Obtiene aleatoriamente algún vértice que pertenezca a la comisaria pasada por parametro
     *
     * @param resourceType Tipo de recurso policial. 1-Patrullera, 2-Motocicleta, 3-Oficial a pie
     * @param comisariaId  Id de la comisaria
     * @return Id del vértice correspondiente a la comisaria
     */
    public Integer getVertexByResourceAndDepartment(Integer resourceType, Integer comisariaId) {
        Integer result = null;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select * from (" +
                "select " +
                "distinct v.id " +
                "from " +
                scheme + ".vertex v " +
                "join " + scheme + ".edge e on e.origin_id = v.id " +
                "where " +
                "v.comisaria_id = ? ";
        // Si es una patrullera o motocicleta, evitar retornar un nodo donde solo pueda transitar un oficial a pie
        if (resourceType.equals(PATROL) || resourceType.equals(MOTORCYCLE)) {
            selectSQL += "and e.patrol_time <> -1 ";
        }
        selectSQL += ") as x " +
                "ORDER by random() " +
                "limit 1";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, comisariaId);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    /**
     * Obtiene el ID del resultado correspondiente a ubicar el tipo de recurso en el vértice en particular
     *
     * @param resourceType Tipo de recurso policial. 1-Patrullera, 2-Motocicleta, 3-Oficial a pie
     * @param idVertex     ID del vértice donde se posiciona el recurso policial
     * @return Identificador del resultado
     */
    public Integer getResultIdByVertexPolice(Integer resourceType, Integer idVertex) {
        Integer result = null;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select r.id from " + scheme + ".results r " +
                "where r.police_resource_type = ? AND r.vertex_id = ? ";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            preparedStatement.setInt(1, resourceType);
            preparedStatement.setInt(2, idVertex);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public Integer getTotalEdge() {
        Integer result = null;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select count(id) as violence_level from " + scheme + ".edge;";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt("violence_level");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public Double getLeTotal() {
        if (leTotal == null) {
            leTotal = calculateLeTotal();
        }
        return leTotal;
    }

    private Double calculateLeTotal() {
        double result = 0.0;
        PreparedStatement preparedStatement = null;
        String selectSQL = "select sum(violence_level) as violence_level from " + scheme + ".edge;";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getDouble("violence_level");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public double getHotSpotFunction(List<Integer> resultIdList) {
        String selectSQL = "select sum(r.violence_level) as violence " +
                "from " + scheme + ".results r " +
                "where r.id = ANY(?)";
        return executeQueryDouble(selectSQL, resultIdList);
    }

    /**
     * Obtiene la suma del nivel de violencia asociado a esos resultados
     *
     * @param resultIdList lista de IDs de resultado
     * @return valor de nivel de violencia cubierto
     */
    public Integer getEdgeCovered(List<Integer> resultIdList) {
        Integer result = null;
        String selectSQL = "select count(distinct re.edge_id) as violence from " + scheme + ".results_edge re join "
                + scheme + ".edge e on re.edge_id = e.id where re.result_id = ANY(?)";
        PreparedStatement preparedStatement = null;
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            Array array = dbConnection.createArrayOf("int4", resultIdList.toArray());
            preparedStatement.setArray(1, array);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getInt("violence");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    /**
     * Obtiene la suma del nivel de violencia asociado a esos resultados
     *
     * @param resultIdList lista de IDs de resultado
     * @return valor de nivel de violencia cubierto
     */
    public double getLeCovered(List<Integer> resultIdList) {
        String selectSQL = "select sum(violence_level) as violence from (select distinct re.edge_id, e.violence_level " +
                "from " + scheme + ".results_edge re join " + scheme + ".edge e on re.edge_id = e.id where re.result_id " +
                "= ANY(?)) as temp";
        return executeQueryDouble(selectSQL, resultIdList);
    }

    /**
     * Obtiene un valor double dado una consulta y una lista de ids
     *
     * @param selectSQL    Consulta a ser ejecutada
     * @param resultIdList Lista de Ids a ser utilizada por la consulta
     * @return resultado de la ejecucion de la consulta
     */
    private double executeQueryDouble(String selectSQL, List<Integer> resultIdList) {
        double result = 0.0;
        PreparedStatement preparedStatement = null;
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL);
            Array array = dbConnection.createArrayOf("int4", resultIdList.toArray());
            preparedStatement.setArray(1, array);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                result = rs.getDouble("violence");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return result;
    }

    public void insertSolution(String instance, double fCob, long executionTime, List<Integer> resultsIdList,
                               double fRec, int edgeNotCovered) throws Exception {
        PreparedStatement preparedStatement = null;
        String selectSQL = " INSERT INTO " + scheme + ".solutions(instance, solution_type, f_cob, execution_time, " +
                "f_rec, edge_not_covered) VALUES (?, ?, ?, ?, ?, ?);";
        try {
            Connection dbConnection = database.getDBConnection();
            preparedStatement = dbConnection.prepareStatement(selectSQL, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, instance);
            preparedStatement.setString(2, "A1");
            preparedStatement.setDouble(3, fCob);
            preparedStatement.setDouble(4, executionTime);
            preparedStatement.setDouble(5, fRec);
            preparedStatement.setInt(6, edgeNotCovered);
            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new SQLException("Creating Solution failed, no rows affected.");
            }

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                insertSolutionResult(resultsIdList, generatedKeys.getInt(1));
            } else {
                throw new SQLException("Creating Solution failed, no ID obtained.");
            }
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    private void insertSolutionResult(List<Integer> resultsIdList, Integer idSolution) throws Exception {
        PreparedStatement preparedStatement = null;
        String selectSQL = " INSERT INTO " + scheme + ".solutions_results(solution_id, result_id) VALUES (?, ?);";
        try {
            Connection dbConnection = database.getDBConnection();
            for (Integer idResult : resultsIdList) {
                preparedStatement = dbConnection.prepareStatement(selectSQL);
                preparedStatement.setInt(1, idSolution);
                preparedStatement.setInt(2, idResult);
                int affectedRows = preparedStatement.executeUpdate();
                if (affectedRows == 0) {
                    throw new SQLException("Creating Solution failed, no rows affected.");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
}
